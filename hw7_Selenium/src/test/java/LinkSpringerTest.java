import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import springer.POMs.AdvancedSearchPage;
import springer.POMs.IndexPage;
import springer.POMs.LoginPage;
import springer.POMs.SearchPage;
import springer.article.Article;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LinkSpringerTest {
    private final static WebDriver driver = new FirefoxDriver();

    private void rejectCookies() {
        new WebDriverWait(driver, Duration.ofSeconds(3)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[data-cc-action='reject']"))).click();
    }

    @Test
    public void test_goToLogin_fromHomePage() {
        IndexPage indexPage = new IndexPage(driver);
        driver.get(indexPage.getURL());

        // if logged in, log out, or else the test doesn't make sense
        if(indexPage.isLoggedIn()) {
            indexPage.logOut();
            }

        try {
            rejectCookies();
        } catch (TimeoutException e) {
            // cookies already accepted
        }
        LoginPage loginPage = indexPage.goToLoginPage();
        loginPage.enterEmailAndPassword("dustcraftcz@gmail.com", "123456789lol");
        assertEquals("https://link.springer.com/", driver.getCurrentUrl());
    }

    @ParameterizedTest
    @CsvSource({"5", "3", "2"})
    public void searchTest_findNArticlesThroughAdvancedSearchAndCompareThemToBasicSearchArticles(int count){
        IndexPage homePage = new IndexPage(driver);
        driver.get(homePage.getURL());

        if (!homePage.isLoggedIn()) {
            rejectCookies();
            homePage.goToLoginPage().enterEmailAndPassword("dustcraftcz@gmail.com", "123456789lol");
            assertTrue(homePage.isLoggedIn());
        }

        // get articles
        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver);
        driver.get(advancedSearchPage.getURL());
        advancedSearchPage.enterAllWords("Page Object Model");
        advancedSearchPage.enterAtLeastOneOfWords("Selenium Testing");
        advancedSearchPage.selectYear("2024");
        advancedSearchPage.clickSearchButton();
        SearchPage searchPage = new SearchPage(driver);
        searchPage.filterByArticles();
        List<Article> articles = searchPage.getArticles(count);

        // search article
        driver.get(searchPage.getURL());
        searchPage = new SearchPage(driver);

        for (Article article : articles) {
            driver.get(searchPage.getURL());
            Article searchedArticle = searchPage.findArticle(article.getTitle());
            assertEquals(searchedArticle.getDoi(), article.getDoi());
            assertEquals(searchedArticle.getPublicationDate(), article.getPublicationDate());
            assertEquals(searchedArticle.getTitle(), article.getTitle());
        }
    }

    @AfterAll
    public static void tearDown() {
        driver.quit();
    }
}
