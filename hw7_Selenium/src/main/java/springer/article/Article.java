package springer.article;

import java.util.Objects;

public class Article {
    private String title;
    private String doi;
    private String publicationDate;

    public Article(String title, String doi, String publicationDate) {
        this.title = title;
        this.doi = doi;
        this.publicationDate = publicationDate;
    }

    public String getTitle() {
        return title;
    }

    public String getDoi() {
        return doi;
    }

    public String getPublicationDate() {
        return publicationDate;
    }
}

