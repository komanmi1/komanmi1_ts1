package springer.POMs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class IndexPage {

    private final WebDriver driver;

    @FindBy(id = "identity-account-widget")
    private WebElement loginButton;

    public IndexPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public LoginPage goToLoginPage() {
        loginButton.click();
        return new LoginPage(driver);
    }

    public boolean isLoggedIn() {
        return new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a#identity-account-widget span")))
                .getText().equals("Account");
    }

    public void logOut() {
        new WebDriverWait(driver, Duration.ofSeconds(3)).until(ExpectedConditions.presenceOfElementLocated(By.ById.id("identity-account-widget"))).click();
        new WebDriverWait(driver, Duration.ofSeconds(3)).until(ExpectedConditions.presenceOfElementLocated(By.ByCssSelector.cssSelector("#account-nav-menu > ul > li:nth-child(4) > a"))).click();
    }

    public String getURL() {
        return "https://link.springer.com";
    }
}
