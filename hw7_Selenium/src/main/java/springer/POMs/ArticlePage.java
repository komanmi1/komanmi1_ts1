package springer.POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ArticlePage {
    @FindBy(css = "h1[data-test='article-title']")
    WebElement articleTitle;

    @FindBy(css = "span.c-bibliographic-information__value")
    List<WebElement> doi;

    @FindBy(css = "time")
    List<WebElement> publicationDate;

    public ArticlePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public String getArticleTitle() {
        return articleTitle.getText();
    }

    public String getDoi() {
        return doi.get(doi.size()-1).getText();
    }

    public String getPublicationDate() {
        return publicationDate.get(0).getAttribute("datetime");
    }

    public String getUrl() {
        return "https://link.springer.com/article/";
    }
}
