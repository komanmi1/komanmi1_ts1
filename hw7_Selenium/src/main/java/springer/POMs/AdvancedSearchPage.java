package springer.POMs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class AdvancedSearchPage {
    private final WebDriver driver;

    @FindBy(id="all-words")
    private WebElement allWordsInput;

    @FindBy(id="least-words")
    private WebElement atLeastOneOfWordsInput;

    @FindBy(id="date-facet-mode")
    private WebElement dateFacetMode;

    @FindBy(id="facet-start-year")
    private WebElement startYear;

    @FindBy(id="submit-advanced-search")
    private WebElement searchButton;

    public AdvancedSearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickSearchButton() {
        searchButton.click();
    }

    public void enterAllWords(String allWords) {
        allWordsInput.sendKeys(allWords);
    }

    public void enterAtLeastOneOfWords(String leastWords) {
        atLeastOneOfWordsInput.sendKeys(leastWords);
    }

    public void selectYear(String year) {
        new Select(dateFacetMode).selectByValue("in");
        this.startYear.sendKeys(year);
    }

    public String getURL() {
        return "https://link.springer.com/advanced-search/";
    }

    public void rejectCookies() {
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a#identity-account-widget span"))).click();
    }
}
