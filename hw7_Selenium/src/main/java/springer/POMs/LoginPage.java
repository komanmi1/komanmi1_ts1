package springer.POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private final String URL = "https://link.springer.com/signup-login";

    private final WebDriver driver;

    @FindBy(id = "login-email")
    private WebElement emailTextField;

    @FindBy(id = "login-password")
    private WebElement passwordTextField;

    @FindBy(id = "email-submit")
    private WebElement continueButton;

    @FindBy(id = "password-submit")
    private WebElement loginButton;

    public  LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterEmailAndPassword(String email, String password) {
        emailTextField.sendKeys(email);
        continueButton.click();
        passwordTextField.sendKeys(password);
        loginButton.click();
    }
}
