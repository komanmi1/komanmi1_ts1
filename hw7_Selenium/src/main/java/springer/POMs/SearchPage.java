package springer.POMs;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import springer.article.Article;

import java.util.ArrayList;
import java.util.List;

public class SearchPage {
    private WebDriver driver;

    //    @FindBy(css = "button.eds-c-button.eds-c-button--secondary.app-search__button-filter")
//    private WebElement searchInput;

    public SearchPage(WebDriver driver) {
        this.driver = driver;
//        PageFactory.initElements(driver, this);
    }

    public void filterByArticles() {
        WebElement articlesFilter = driver.findElement(By.cssSelector("label[for='content-type-article']"));
        JavascriptExecutor jsE = (JavascriptExecutor) driver;
        jsE.executeScript("arguments[0].scrollIntoView();", articlesFilter);
        articlesFilter.click();
        WebElement updateButton = driver.findElement(By.cssSelector("button.eds-c-button.eds-c-button--primary[form='search-form']"));
        updateButton.click();
    }

    public Article findArticle(String articleTitle) {
        driver.findElement(By.id("search-springerlink")).sendKeys(articleTitle);
        driver.findElement(By.id("search-submit")).click();
        List<WebElement> articlesWeb = driver.findElement(By.cssSelector("ol[data-test='darwin-search']"))
                .findElements(By.cssSelector("li[data-test='search-result-item']"));
        articlesWeb.get(0).click();

        ArticlePage articlePage = new ArticlePage(driver);
        return new Article(articlePage.getArticleTitle(), articlePage.getDoi(), articlePage.getPublicationDate());
    }

    public List<Article> getArticles(int count) {
        WebElement articlesElement = driver.findElement(By.cssSelector("ol[data-test='darwin-search']"));
        List<WebElement> articlesWeb = articlesElement.findElements(By.cssSelector("li[data-test='search-result-item']"));
        List<Article> articles = new ArrayList<>();

        for(int i = 0; i < Math.min(count, articlesWeb.size()) ; i++){
            WebElement article = articlesWeb.get(i);
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView();", article);
            article.click();

            ArticlePage articlePage = new ArticlePage(driver);
            articles.add(new Article(articlePage.getArticleTitle(), articlePage.getDoi(), articlePage.getPublicationDate()));
            driver.navigate().back();
            articlesElement = driver.findElement(By.cssSelector("ol[data-test='darwin-search']"));
            articlesWeb = articlesElement.findElements(By.cssSelector("li[data-test='search-result-item']"));
        }
        return articles;
    }

    public String getURL() {
        return "https://link.springer.com/search";
    }
}
