import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {
    @ParameterizedTest
    @CsvSource({
        "1, 1, 2",
        "2, 3, 5",
        "3, 3, 6"
    })
    public void testAdd(int a, int b, int c) {
        Calculator calculator = new Calculator();
        assertEquals(c, calculator.add(a, b));
    }

    @ParameterizedTest
    @CsvSource({
        "1, 1, 0",
        "2, 3, -1",
        "3, 3, 0"
    })
    public void testSubtract(int a, int b, int c) {
        Calculator calculator = new Calculator();
        assertEquals(c, calculator.subtract(a, b));
    }

    @ParameterizedTest
    @CsvSource({
        "1, 1, 1",
        "2, 3, 6",
        "3, 3, 9"
    })
    public void testMultiply(int a, int b, int c) {
        Calculator calculator = new Calculator();
        assertEquals(c, calculator.multiply(a, b));
    }

    @ParameterizedTest
    @CsvSource({
        "12, 3, 4",
        "12, 4, 3",
        "12, 2, 6"
    })
    public void testDivide(int a, int b, int c) {
        Calculator calculator = new Calculator();
        assertEquals(c, calculator.divide(a, b));
    }

    @Test
    public void testDivideByZero() {
        Calculator calculator = new Calculator();
        assertThrows(IllegalArgumentException.class, () -> {
            calculator.divide(1, 0);
        });
    }
}
