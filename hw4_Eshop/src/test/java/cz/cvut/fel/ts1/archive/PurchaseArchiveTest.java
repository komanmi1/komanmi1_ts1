package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class PurchaseArchiveTest {
    @Test
    public void purchaseStatistics_printTest() {
        ArrayList<ItemPurchaseArchiveEntry> itemPurchaseArchiveEntriesEntries = new ArrayList<>(5);
        ArrayList<Item> items = new ArrayList<>(5);
        for (int i = 0; i < 5; i++) {
            Item item = new StandardItem(i, "item" + i, i*1.1F, "category" + i, 22*i);
            items.add(item);
            itemPurchaseArchiveEntriesEntries.add(ItemPurchaseArchiveEntry.createItemPurchaseArchiveEntry(item));
        }
        PurchasesArchive purchasesArchive = new PurchasesArchive();

        PrintStream oldOut = System.out;
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        StringBuilder expectedOutput = new StringBuilder("ITEM PURCHASE STATISTICS:");
        for(ItemPurchaseArchiveEntry e : itemPurchaseArchiveEntriesEntries) {
            expectedOutput.append('\n');
            expectedOutput.append(e.toString());
        }

        purchasesArchive.putOrderToPurchasesArchive(new Order(new ShoppingCart(items), "customer", "address", 1));
        purchasesArchive.printItemPurchaseStatistics();

        assertEquals(expectedOutput.toString(), outputStreamCaptor.toString().trim());

        System.setOut(oldOut);
    }

    @Test
    public void getHowManyTimesHasBeenItemSoldTest() {
        Item item = new StandardItem(1, "item", 1.1F, "category", 22);
        PurchasesArchive purchasesArchive = new PurchasesArchive();
        ArrayList<Item> items = new ArrayList<>(Arrays.asList(item, item, item));
        purchasesArchive.putOrderToPurchasesArchive(new Order(new ShoppingCart(items), "customer", "address", 1));

        assertEquals(3, purchasesArchive.getHowManyTimesHasBeenItemSold(item));
    }

    @Test
    public void getHowManyTimesHasBeenItemSold_mockedItemPurchaseArchiveEntry() {
        Item item = mock(Item.class);
        ItemPurchaseArchiveEntry entry = mock(ItemPurchaseArchiveEntry.class);

        when(item.getID()).thenReturn(1);
        when(entry.getCountHowManyTimesHasBeenSold()).thenReturn(5);

        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchive = new HashMap<>();
        itemPurchaseArchive.put(1, entry);
        PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchive, new ArrayList<>());

        int timesSold = archive.getHowManyTimesHasBeenItemSold(item);

        assert(timesSold == 5);
    }

    @Test
    public void putOrderToPurchasesArchive_mockedOrderArchiveTest() {
        Order order = mock(Order.class);
        Item item1 = mock(Item.class);
        Item item2 = mock(Item.class);

        when(item1.getID()).thenReturn(1);
        when(item2.getID()).thenReturn(2);

        ArrayList<Item> orderItems = new ArrayList<>();
        orderItems.add(item1);
        orderItems.add(item2);

        when(order.getItems()).thenReturn(orderItems);

        ArrayList<Order> orderArchiveMock = mock(ArrayList.class);

        PurchasesArchive archive = new PurchasesArchive(new HashMap<Integer, ItemPurchaseArchiveEntry>(), orderArchiveMock);

        archive.putOrderToPurchasesArchive(order);
        archive.putOrderToPurchasesArchive(order);
        archive.putOrderToPurchasesArchive(order);

        verify(orderArchiveMock, times(3)).add(order);
    }
}
