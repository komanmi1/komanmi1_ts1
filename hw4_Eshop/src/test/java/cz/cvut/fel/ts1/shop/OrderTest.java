package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class OrderTest {
    @Test
    public void orderConstructorTest() {
        ArrayList<Item> items = new ArrayList<>(4);
        items.add(new StandardItem(1, "std item", 3685.99F, "category", 10));
        items.add(new StandardItem(2, "std item2", 251.11F, "category_two", 67));
        items.add(new StandardItem(3, "std item3", 0.0F, "category_three", 1110));
        items.add(new StandardItem(4, "std item4", 1.6F, "category_four", 1));

        ShoppingCart cart = new ShoppingCart(items);
        String customerName = "customer";
        String customerAddress = "address";
        int state = 1;

        Order order = new Order(cart, customerName, customerAddress, state);
        assertEquals(cart.getCartItems(), order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(state, order.getState());
    }

    @Test
    public void orderConstructor_NullCart_ExceptionThrown() {
        ShoppingCart shoppingCart = null;
        String customerName = "customer";
        String customerAddress = "address";
        int state = 1;

        assertThrows(IllegalArgumentException.class, () -> new Order(shoppingCart, customerName, customerAddress, state));
    }
}
