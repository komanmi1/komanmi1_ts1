package cz.cvut.fel.ts1.shop;

import cz.cvut.fel.ts1.storage.NoItemInStorageException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class EShopControllerTest {
    @Test
    public void emptyCartTest_throwsEmptyCartException() {
        EShopController.startEShop();
        ShoppingCart cart = EShopController.newCart();
        cart.addItem(new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5));
        cart.removeItem(1);
        assertThrows(EmptyCartException.class, () -> EShopController.purchaseShoppingCart(cart, "John Doe", "New York City"));
    }

    @Test
    public void noItemInStorageTest_throwsNoItemInStorageException() {
        EShopController.startEShop();
        ShoppingCart cart = EShopController.newCart();
        cart.addItem(new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5));
        assertThrows(NoItemInStorageException.class, () -> EShopController.purchaseShoppingCart(cart, "John Doe", "New York City"));
    }

    @Test
    public void notEnoughItemsInStorage_throwsNoItemInStorageException() {
        EShopController.startEShop();
        ShoppingCart cart = EShopController.newCart();

        Item item = new StandardItem(7, "Running rabbit", 200, "minifigure", 50);
        cart.addItem(item);
        cart.addItem(item);

        EShopController.addItemsToStorage(item, 1);
        assertThrows(NoItemInStorageException.class, () -> EShopController.purchaseShoppingCart(cart, "John Doe", "New York City"));
    }

    @Test
    public void standardShoppingTest() {
        EShopController.startEShop();
        ShoppingCart cart = EShopController.newCart();

        ArrayList<Item> items = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            Item item = new StandardItem(i, "Dancing Panda v."+i, 500*i, "GADGET no."+i, 23*i);
            cart.addItem(item);
            items.add(item);
            EShopController.addItemsToStorage(item, 1);
        }

        assertEquals(items.size(), cart.getItemsCount());
        items.remove(2);
        cart.removeItem(2);
        assertEquals(items.size(), cart.getItemsCount());

        ArrayList<Item> shoppingCartItems = cart.getCartItems();
        for(int i = 0; i < items.size(); i++) {
            assertEquals(items.get(i), shoppingCartItems.get(i));
        }

        float priceSum = 0;
        for (Item item : items) {
            priceSum += item.getPrice();
        }
        assertEquals(priceSum, cart.getTotalPrice());

        assertDoesNotThrow(() -> EShopController.purchaseShoppingCart(cart, "John Doe", "New York City"));
    }

    @Test
    public void discountedShoppingTest() {
        EShopController.startEShop();
        ShoppingCart cart = EShopController.newCart();

        ArrayList<Item> discountedItems = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            Item item = null;
            try {
                item = new DiscountedItem(i, "Dancing Panda v." + i, 500 * i, "GADGET no." + i, 2*i, "1.1.2024", "31.12.2024");
            } catch (IllegalDiscountException e) {
                assert false;
            }
            cart.addItem(item);
            discountedItems.add(item);
            EShopController.addItemsToStorage(item, 1);
        }

        cart.getTotalPrice();

        assertEquals(discountedItems.size(), cart.getItemsCount());
        discountedItems.remove(7);
        cart.removeItem(7);
        assertEquals(discountedItems.size(), cart.getItemsCount());

        ArrayList<Item> shoppingCartItems = cart.getCartItems();
        for(int i = 0; i < discountedItems.size(); i++) {
            assertEquals(discountedItems.get(i), shoppingCartItems.get(i));
        }

        float priceSum = 0;
        for (Item item : discountedItems) {
            priceSum += ((DiscountedItem)item).getDiscountedPrice();
        }
        assertEquals(priceSum, cart.getTotalPrice());

        assertDoesNotThrow(() -> EShopController.purchaseShoppingCart(cart, "John Doe", "New York City"));
    }

    @Test
    public void randomShoppingTest() {
        EShopController.startEShop();
        ShoppingCart cart = EShopController.newCart();

        ArrayList<Item> normalItems = new ArrayList<>(10);
        ArrayList<Item> discountedItems = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            Item item = new StandardItem(i, "Dancing Pikachu v."+i, 40*i, "Pokeball no."+i, 2*i);
            cart.addItem(item);
            normalItems.add(item);
            EShopController.addItemsToStorage(item, i%2 == 0? 5 : 1);

            Item discountedItem = null;
            try {
                discountedItem = new DiscountedItem(i+10, "Dancing Thor, weight in tons: " + i, 90 * i, "Mjollnir no." + i, 4*i, "1.1.2024", "31.12.2024");
            } catch (IllegalDiscountException e) {
                assert false;
            }
            cart.addItem(discountedItem);
            discountedItems.add(discountedItem);
            EShopController.addItemsToStorage(discountedItem, i%2 == 0? 5 : 1);
        }

        assertEquals(normalItems.size() + discountedItems.size(), cart.getItemsCount());
        normalItems.remove(2);
        discountedItems.remove(7);
        discountedItems.remove(3);
        cart.removeItem(2);
        cart.removeItem(17);
        cart.removeItem(13);
        assertEquals(normalItems.size() + discountedItems.size(), cart.getItemsCount());

        float totalPrice = 0;
        for(Item item : normalItems){
            totalPrice += item.getPrice();
        }
        for(Item item : discountedItems){
            totalPrice += ((DiscountedItem)item).getDiscountedPrice();
        }

        assertEquals(totalPrice, cart.getTotalPrice());
        assertDoesNotThrow(() -> EShopController.purchaseShoppingCart(cart, "John Doe", "New York City"));
    }
}
