package cz.cvut.fel.ts1.storage;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemStockTest {
    @Test
    public void itemStockConstructorTest() {
        Item item = new StandardItem(1, "item", 1, "category", 1);
        ItemStock itemStock = new ItemStock(item);

        assertEquals(item, itemStock.getItem());
        assertEquals(0, itemStock.getCount());
    }

    @ParameterizedTest
    @CsvSource({
            "1",
            "5",
            "6",
            "10",
    })
    public void itemStockIncreaseItemCountTest(int increaseAmount) {
        Item item = new StandardItem(1, "item", 1, "category", 1);
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(increaseAmount);
        assertEquals(increaseAmount, itemStock.getCount());
    }

    @ParameterizedTest
    @CsvSource({
            "1",
            "3",
            "5",
            "7",
    })
    public void itemStockDoubleIncreaseItemCountTest(int increaseAmount) {
        Item item = new StandardItem(1, "item", 1, "category", 1);
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(increaseAmount);
        itemStock.IncreaseItemCount(increaseAmount);
        assertEquals(2*increaseAmount, itemStock.getCount());
    }

    @ParameterizedTest
    @CsvSource({
            "11, 1",
            "7, 2",
            "18, 6",
            "10, 9",
    })
    public void itemStockDecreaseItemCountTest(int defaultStockAmount,int decreaseAmount) {
        Item item = new StandardItem(1, "item", 1, "category", 1);
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(defaultStockAmount);
        itemStock.decreaseItemCount(decreaseAmount);
        assertEquals(defaultStockAmount - decreaseAmount, itemStock.getCount());
    }

    @ParameterizedTest
    @CsvSource({
            "11, 1",
            "7, 2",
            "18, 6",
            "20, 9",
    })
    public void itemStockDoubleDecreaseItemCountTest(int defaultStockAmount,int decreaseAmount) {
        Item item = new StandardItem(1, "item", 1, "category", 1);
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(defaultStockAmount);
        itemStock.decreaseItemCount(decreaseAmount);
        itemStock.decreaseItemCount(decreaseAmount);
        assertEquals(defaultStockAmount - 2*decreaseAmount, itemStock.getCount());
    }
}
