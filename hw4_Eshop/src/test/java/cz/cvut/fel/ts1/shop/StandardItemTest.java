package cz.cvut.fel.ts1.shop;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class StandardItemTest {
    @Test
    public void standardItemConstructorTest() {
        int id = 1;
        String name = "std item";
        float price = 3685.99F;
        String category = "category";
        int loyaltyPoints = 10;

        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        assertEquals(id, standardItem.getID());
        assertEquals(name, standardItem.getName());
        assertEquals(price, standardItem.getPrice());
        assertEquals(category, standardItem.getCategory());
        assertEquals(loyaltyPoints, standardItem.getLoyaltyPoints());

        String superStandardItemToString = "Item   ID "+id+"   NAME "+name+"   CATEGORY "+category;
        String standardItemToString = superStandardItemToString+"   PRICE "+price+"   LOYALTY POINTS "+loyaltyPoints;
        assertEquals(standardItemToString, standardItem.toString());
    }

    @Test
    public void StandardItemCopyTest() {
        int id = 2;
        String name = "std item2";
        float price = 251.11F;
        String category = "category_two";
        int loyaltyPoints = 67;

        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem standardItemCopy = standardItem.copy();

        assertEquals(standardItem.getID(), standardItemCopy.getID());
        assertEquals(standardItem.getName(), standardItemCopy.getName());
        assertEquals(standardItem.getPrice(), standardItemCopy.getPrice());
        assertEquals(standardItem.getCategory(), standardItemCopy.getCategory());
        assertEquals(standardItem.getLoyaltyPoints(), standardItemCopy.getLoyaltyPoints());
    }

    @ParameterizedTest
    @CsvSource({
            "1, 'std item', 3685.99, 'category', 10",
            "12, 'std item12', 251.11, 'category_twelve', 67",
            "21, 'std item21', 0.0, 'category_twentyone', 1110",
            "4, 'std item4', 1.6, 'category_four', 1",
    })
    public void StandardItemEqualsTest(int id, String name, float price, String category, int loyaltyPoints) {
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem standardItemCopy = standardItem.copy();
        assertEquals(standardItem, standardItemCopy);
    }


}
