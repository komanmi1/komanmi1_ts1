package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

public class ItemPurchaseArchiveEntryTest {
    @Test
    public void purchaseArchiveEntry_constructedObjectTest() {
        Item item = new StandardItem(1, "item", 1.1F, "category", 22);

        Order order = new Order(new ShoppingCart(new ArrayList<>(Arrays.asList(item))), "customer", "address", 1);

        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchive = new HashMap<>();
        PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchive, new ArrayList<>());
        archive.putOrderToPurchasesArchive(order);

        assertEquals(item, itemPurchaseArchive.get(1).getRefItem());
        assertEquals(1, itemPurchaseArchive.get(1).getCountHowManyTimesHasBeenSold());
    }

    @Test
    public void purchaseArchiveEntry_simpleConstructorTest() {
        Item item = new StandardItem(1, "item", 1.1F, "category", 22);

        ItemPurchaseArchiveEntry entry = ItemPurchaseArchiveEntry.createItemPurchaseArchiveEntry(item);

        assertEquals(item, entry.getRefItem());
        assertEquals(1, entry.getCountHowManyTimesHasBeenSold());
    }

    static class WrongArgumentException extends Exception {}

    @Test
    public void purchaseArchiveEntry_constructorCallArgumentsTest() {
        Item item = new StandardItem(1, "item", 1.1F, "category", 11);

//        For purpose of failing this test to confirming that it makes sense.
//        Item item2 = new StandardItem(666, "fail item", 6.66F, "failing category", 666);

        Order order = new Order(new ShoppingCart(new ArrayList<>(Arrays.asList(item))), "customer", "address", 1);

        try(MockedStatic<ItemPurchaseArchiveEntry> itemPurchaseArchiveEntryMockedStatic= Mockito.mockStatic(ItemPurchaseArchiveEntry.class)) {
            itemPurchaseArchiveEntryMockedStatic.when(() -> ItemPurchaseArchiveEntry.createItemPurchaseArchiveEntry(any(Item.class)))
                    .thenThrow(new WrongArgumentException());
            itemPurchaseArchiveEntryMockedStatic.when(() -> ItemPurchaseArchiveEntry.createItemPurchaseArchiveEntry(item))
                    .thenReturn(new ItemPurchaseArchiveEntry(item));

            PurchasesArchive archive = new PurchasesArchive();
            archive.putOrderToPurchasesArchive(order);
        }
    }
}
