package cz.cvut.fel.ts1.storage;

public class NoItemInStorageException extends Exception{
    public NoItemInStorageException() {
        super("No item in storage");
    }
}
