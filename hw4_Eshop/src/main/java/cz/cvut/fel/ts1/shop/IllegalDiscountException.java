package cz.cvut.fel.ts1.shop;

public class IllegalDiscountException extends Exception{
    public IllegalDiscountException() {
        super("Discount must be an integer between 0 and 100!");
    }
}
