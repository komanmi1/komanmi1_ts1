package cz.cvut.fel.ts1.shop;

public class EmptyCartException extends Exception {
    public EmptyCartException() {
        super("Cart is empty!");
    }
}
